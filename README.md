*Shell execution*
> python3 ./datapet_yaml_generator.py https://url.to.splunk.instance splunkUsername splunkPassword indexToSearch sourcetypeToSearch --timestampformat="%d/%b/%Y %H:%M:%S" --outputdir=/tmp/

*Shell execution example*
> python3 ./datapet_yaml_generator.py https://i-04381dce585d52d14.splunk.show admin redacted itsidemo tomcat:access:log --earliest=-4h@h --timestampformat="%d/%b/%Y %H:%M:%S"


*How it Works*
* Search target index and sourcetype over last 60m (use --earliest option to change)
* By searching Splunk, obtain a list of _raw samples to be used as the -lines in the yaml. One _raw per punct because punct is a decent way to determine the different types of sample data in the sourcetype
* By using some excellent SPL magic using the `fieldsummary` command, extract out the fields that need to be tokenized in the _raw for the -tokens section of the yaml
* Iterate through the _raw examples and attempt to tokenize as much of the _raw as possible
* Dump the final results out to the --outputdir location and print to the screen any manual updates needed to the yaml

*Caveats*
* You will almost certainly need to make minor cleanups to the yaml
* The resulting eventgen produces randomized results... no attempt was made to enforce ordering or weighting of events and tokens