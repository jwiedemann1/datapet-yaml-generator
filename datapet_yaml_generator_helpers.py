import os
import subprocess
import json
import yaml
import csv

def getShellOutput(cmd):
    return subprocess.check_output(cmd, shell=True)

class yamlMultilineStr(str):
    pass

def yamlMultilineStr_representer(dumper, data):
        style = '|'
        # if sys.versioninfo < (3,) and not isinstance(data, unicode):
        #     data = unicode(data, 'ascii')
        tag = u'tag:yaml.org,2002:str'
        return dumper.represent_scalar(tag, data, style=style)

yaml.add_representer(yamlMultilineStr, yamlMultilineStr_representer, Dumper=yaml.SafeDumper)

def returnSplunkSearch(args, dataDict, search):
    search = search.format(**dataDict)
    search = search.replace("\n", " ").replace("\"", "\\\"")

    cmd = "curl -ku {0}:{1} {2}:8089/servicesNS/admin/search/search/jobs/export --data-urlencode search=\"{3}\" -d preview=false -d output_mode=json".format(args.splunkUsername, args.splunkPassword, args.splunkInstance, search)

    #print (cmd)

    results = getShellOutput(cmd)

    #print(results)

    ret = []

    for result in results.splitlines():
        result = result.decode("UTF-8")
        if ("result" in result):
            try:
                ret.append(json.loads(result)["result"])
            except:
                print("Failed to parse results as json. Expecting a result field")
                print (cmd)
                print(result)
                return

    return ret

def writeYamlProject(args, fullCommandLine, sampleYaml):
    finalContainerDir = "{}/{}".format(args.outputdirroot, args.outputdir)

    from pathlib import Path
    Path(finalContainerDir).mkdir(parents=True, exist_ok=True)

    #Write tokens to file if the yaml contains any
    for token in sampleYaml["samples"][0]["tokens"]:
        if token.get("sample", None):
            open("{0}/{1}.sample".format(finalContainerDir, token["name"]), 'w').write("\n".join(token["sample"]))
            token["sample"] = "{0}.sample".format(token["name"])
        

    #Write the yaml to file
    yml_file_name = "{0}/{1}.yml".format(finalContainerDir, pathsafe(sampleYaml["samples"][0]["name"]))
    open(yml_file_name, 'w').write(yaml.safe_dump(sampleYaml, sort_keys=False))
    print ("Wrote yml to:\n{0}\n".format(yml_file_name))

    cmdline_file = "{0}/{1}_fullcommandline.txt".format(finalContainerDir, pathsafe(sampleYaml["samples"][0]["name"]))
    open(cmdline_file, 'w').write(fullCommandLine)

def tokenValueExistsInRaw(token, lines):
    for value in token["values"].split("::::"):
        for line in lines:
            for wrapper in [("\"", "\""), ("=", ""), (":", ""), (">", "<"), (" ", ","), (" ", " "), ("[", "]")]:
                val="{0}{1}{2}".format(wrapper[0], value, wrapper[1])
                if (val in line["tokenized_raw"] and value!=""):
                    return (True)

    return (False)

def tokenizeRaw(raw, splitbyValue, tokens):
    tokenizedRaw = raw.replace("TIMESTAMP-TOKEN", "$TIMESTAMP$")

    for t in tokens:
        for s in t.get("sample", []):
            if (s != splitbyValue):
                for wrapper in [("\"", "\""), ("=", ""), (":", ""), (">", "<"), (" ", ","), (" ", " "), ("[", "]")]:
                    val="{0}{1}{2}".format(wrapper[0], s, wrapper[1])
                    tokenizedRaw = tokenizedRaw.replace(val, "{0}${1}${2}".format(wrapper[0], t["name"], wrapper[1]))

    return tokenizedRaw

def coalesceTokens(allTokensDict, token):
    existingToken = allTokensDict.get(token["name"], None)

    if not existingToken:
        allTokensDict.update({token["name"]:token})
        return

    intersect = set(existingToken.get("sample", [])) & set(token.get("sample", []))
    if not intersect:
        print ("Found two tokens of the same name for which there are no overlapping token values... It's possible that these should be broken out as separate tokens. For now, the system has decided to merge them together")
        print (existingToken)
        print (token)

    existingToken["sample"] = list(set(existingToken["sample"]) | set(token["sample"]))
    return

def pathsafe(pathstr):
    return pathstr.replace(":", "-").replace("/", "-").replace("\\", "-").replace("*", "*")

def processReplacements(replacementsCSV, y):
    if not replacementsCSV:
        return y
    
    with open(replacementsCSV, 'r') as csvfile:
        replacements = csv.DictReader(csvfile)

        for row in replacements:
            #Process replacements over token sample values
            for token in y["samples"][0]["tokens"]:
                if token.get("sample", None):
                    token["sample"] = [s.replace(row["find"], row["replace"]) for s in token["sample"]]

            #Process replacements over lines
            for line in y["samples"][0]["lines"]:
                line["_raw"] = line["_raw"].replace(row["find"], row["replace"])
                line["host"] = line["host"].replace(row["find"], row["replace"])
                line["source"] = line["source"].replace(row["find"], row["replace"])
                if line.get("fields", None): line["fields"] = line["fields"].replace(row["find"], row["replace"])