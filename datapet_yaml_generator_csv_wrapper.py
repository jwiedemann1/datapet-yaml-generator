import argparse
import csv
import subprocess
from datapet_yaml_generator_helpers import getShellOutput

def main(args):
	
	with open(args.csvFile, 'r') as csvfile:
		datareader = csv.DictReader(csvfile)

		processingList = [line for line in datareader if line["include"]=="1"]

		for i, row in enumerate(processingList):
			print("Processing [{} of {}]".format(i+1, len(processingList)))

			param_metric_filter = "" if not row["metric_filter"] else "--metrics_filter=\"{}\"".format(row["metric_filter"])
			param_earliest = "" if not row["earliest"] else "--earliest=\"{}\"".format(row["earliest"])
			param_latest = "" if not row["latest"] else "--latest=\"{}\"".format(row["latest"])
			param_outputdirroot = "" if not row["root"] else "--outputdirroot=\"{}\"".format(row["root"])
			param_timestampformat = "" if not row["events_timestampformat"] else "--events_timestampformat=\"{}\"".format(row["events_timestampformat"])
			param_splitby = "" if not row["events_splitby"] else "--events_splitby=\"{}\"".format(row["events_splitby"])
			param_target_entity_filter = "" if not row["target_entity_filter"] else "--target_entity_filter=\"{}\"".format(row["target_entity_filter"])
			param_fields_to_tokenize = "" if not row["fields_to_tokenize"] else "--fields_to_tokenize=\"{}\"".format(row["fields_to_tokenize"])
			param_replacements_csv = "" if not args.replacementsCSV else "--replacementsCSV=\"{}\"".format(args.replacementsCSV)

			cmd = 'python3 ./datapet_yaml_generator_wrapper.py "{}" "{}" "{}" "{}" "{}" "{}" "{}" {} {} {} {} {} {} {} {} {}'.format(args.splunkInstance, args.splunkUsername, args.splunkPassword, row["type"], row["index"], row["sourcetype"], row["folder"], param_metric_filter, param_earliest, param_latest, param_outputdirroot, param_timestampformat, param_splitby, param_target_entity_filter, param_fields_to_tokenize, param_replacements_csv)

			getShellOutput(cmd)


parser = argparse.ArgumentParser()

parser.add_argument("splunkInstance", help="URL of splunk instance to search via rest. (I.E. https://ec2-54-183-21-92.us-west-1.compute.amazonaws.com)")
parser.add_argument("splunkUsername", help="Username")
parser.add_argument("splunkPassword", help="Password")
parser.add_argument("csvFile", help="csv File to process")
parser.add_argument("--replacementsCSV", help="csv File for find-replace", default=None)

main(parser.parse_args())