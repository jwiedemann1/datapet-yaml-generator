from datapet_yaml_generator_helpers import *
from datetime import datetime
import copy

# CONSTANTS
SEARCH_GET_EVENTS_PER_SECOND_FREQUENCY = """| search earliest={earliest} latest={latest} index={index} sourcetype={sourcetype} {target_entity_filter}
| timechart span=1s@s count
| stats avg(count) as frequency
| eval frequency = ceil(frequency)"""

SEARCH_GET_EVENTS_RAW_BY_SOMETHING = """| search earliest={earliest} latest={latest} index={index} sourcetype={sourcetype} {target_entity_filter}
| dedup {splitby}
| eval gmt_offset=strftime(_time, "%z")
| eval token_time=strftime(_time - (gmt_offset*36), "{timestampformat}")
| eval tokenized_raw=replace(_raw, token_time, "TIMESTAMP-TOKEN")
"""

SEARCH_GET_METRICS_RAW = """| mcatalog values(metric_name) as metric_name WHERE earliest={earliest} latest={latest} index=* sourcetype={sourcetype} metric_name IN ({metric_name}) {target_entity_filter} by index, host
| mvexpand metric_name
| addinfo
| streamstats count as inc
| map maxsearches=100000 search="| mstats min(_value) as metric_min_value max(_value) as metric_max_value WHERE metric_name=\\$metric_name\\$ AND index=\\$index\\$ AND host=\\$host\\$ index!="FOOME123-1" {target_entity_filter} earliest=\\$info_min_time\\$ latest=\\$info_max_time\\$ by [|mcatalog values(_dims) as dims where index=\\$index\\$ AND metric_name=\\$metric_name\\$ earliest=\\$info_min_time\\$ latest=\\$info_max_time\\$ host=\\$host\\$ {target_entity_filter} | mvexpand dims | rename dims as search | format \\\\"\\\\", \\\\"\\\\", \\\\"\\\\", \\\\"\\\\", \\\\"\\\\", \\\\"\\\\"] metric_name source sourcetype host index | rename _time as metric_time | fields metric_name source sourcetype host index *"
| sort 0 metric_name source sourcetype host
| dedup 10 metric_name source sourcetype host
| streamstats count as inc
"""

SEARCH_GET_EVENTS_TOKENS = """
| fields - metric_name
| fieldsummary maxvals=300 index {fields_to_tokenize}
| rex field=values "(?<isFloatingPoint>\\.[1-9])"
| rename values as _values
| eval token_type=case(distinct_count < 300, "Category", numeric_count>0 AND (NOT isnull(isFloatingPoint)), "float", numeric_count>0, "int", distinct_count/count > .9 AND distinct_count>100, "Unique", true(), "Unsure")
| eval values=if(token_type="Category", _values, null())
| table field count distinct_count token_type min max values
| eval values=split(values, "}},")
| mvexpand values
| rex field=values "\\\\"value\\\\":\\\\"(?<values>.*?)\\\\","
| search (NOT values="") OR (token_type!=Category)
| eval tmp_len_val = len(values)
| eventstats avg(tmp_len_val) as tmp_len_val by field
| mvcombine values
| eval values = mvjoin(values, "::::")
| sort 0 - tmp_len_val
| fields - tmp_len_val
| eval special_field=if(searchmatch("field IN (host, source, sourcetype, index)"),1,0)
| dedup special_field count distinct_count values
| eval field=upper(field)
| streamstats count as inc by field
| eval field=if(inc>1,field."_".inc,field)"""

def main(args):
    searchData = {
        "index": args.index,
        "sourcetype": args.sourcetype,
        "earliest": args.earliest,
        "latest": args.latest,
        "timestampformat": args.events_timestampformat,
        "splitby": args.events_splitby,
        "target_entity_filter": args.target_entity_filter,
        "metric_name": args.metrics_filter,
        "fields_to_tokenize": args.fields_to_tokenize
    }

    RAW_SEARCH = SEARCH_GET_METRICS_RAW if args.type == "metrics" else SEARCH_GET_EVENTS_RAW_BY_SOMETHING

    #frequency = int(returnSplunkSearch(args, searchData, SEARCH_GET_PER_SECOND_FREQUENCY)[0]["frequency"])
    tokens = returnSplunkSearch(args, searchData, RAW_SEARCH + SEARCH_GET_EVENTS_TOKENS)
    lines = returnSplunkSearch(args, searchData, RAW_SEARCH)

    #The Target YAML
    y = {}

    #The One and Only Sample in this YAML
    yaml_sample = {}
    yaml_sample["name"] = "{}-{}-{}".format(args.outputdir, args.sourcetype, args.type.upper())
    yaml_sample["interval"] = 1 if args.type == "events" else 60
    yaml_sample["count"] = 1
    yaml_sample["randomizeEvents"] = True if args.type == "events" else False
    y["samples"] = [yaml_sample]


    #The Tokens to add to the Sample
    yaml_sample_tokens = []

    for line in lines:
        if line.get("metric_min_value", None):
            fields = copy.copy(line)
            fields["metric_name:{}".format(fields["metric_name"])] = "$VALUE_{0}$".format(line["inc"])
            simulated_metrics_raw = {key: fields[key] for key in fields if key not in ["inc", "metric_name", "host", "source", "sourcetype", "index", "metric_min_value", "metric_max_value"]}
            line["tokenized_raw"] = json.dumps(simulated_metrics_raw, separators=(',', ':'))
            
    for t in tokens:
        yaml_sample_token = {}

        yaml_sample_token["name"] = t["field"].upper()

        if t["token_type"] in ["float", "int"]:
            yaml_sample_token["format"] = "regex"
            yaml_sample_token["token"] = "(?i){}.*?([\\d\\.]+)".format(t["field"])
            yaml_sample_token["type"] = "random"
            yaml_sample_token["replacement"] = t["token_type"]
            yaml_sample_token["lower"] = float(t["min"])
            yaml_sample_token["upper"] = max(float(t["max"]), (float(t["min"])+1.0))    

            yaml_sample_tokens.append(yaml_sample_token)

        elif t["field"].upper()  in ["HOST", "INDEX"]:
            yaml_sample_token["format"] = "template"
            yaml_sample_token["type"] = "choice"
            yaml_sample_token["sample"] = t["values"].split("::::")
            
            yaml_sample_tokens.append(yaml_sample_token)

        elif t["token_type"]=="Category":
            yaml_sample_token["format"] = "template"
            yaml_sample_token["type"] = "choice"
            yaml_sample_token["sample"] = t["values"].split("::::")
            
            if (tokenValueExistsInRaw(t, lines)):
                yaml_sample_tokens.append(yaml_sample_token)

    # Add the timestamp token if it exists in the raw
    for line in lines:
        if "TIMESTAMP" in line["tokenized_raw"]:
            #Manage the timestamp token and tokenizing that in the _raw
            timestamp_token = {
                "name": "TIMESTAMP",
                "format": "template",
                "type": "gotimestamp",
                "replacement": datetime.strptime("2006-01-02 15:04:05 -0700", "%Y-%m-%d %H:%M:%S %z").strftime(args.events_timestampformat) }

            yaml_sample_tokens.append(timestamp_token)
            break

    #For metrics, add all the values tokens too
    for line in lines:
        if line.get("metric_min_value", None):
            token = {}
            token["name"] = "VALUE_{0}".format(line["inc"])
            token["format"] = "template"
            token["field"] = "fields"
            token["type"] = "random"
            token["replacement"] = "float"
            token["lower"] = float(line["metric_min_value"])
            token["upper"] = max(float(line["metric_max_value"]), float(line["metric_min_value"]) + 1)
            yaml_sample_tokens.append(token)

    #Throw all the yaml sample tokens into the larger yaml
    yaml_sample["tokens"] = yaml_sample_tokens

    #The Lines to add to the Sample
    yaml_sample_lines = []
    for i, l in enumerate(lines):
        yaml_sample_line = {}
        yaml_sample_line["index"] = "$INDEX$"
        yaml_sample_line["sourcetype"] = l["sourcetype"]
        yaml_sample_line["source"] = l["source"]
        yaml_sample_line["host"] = "$HOST$" if [t for t in tokens if t["field"].upper()=="HOST"] else l["host"]
        
        #Metrics 
        if args.type == "metrics":
            yaml_sample_line["_raw"] = "metric"
            yaml_sample_line["fields"] = tokenizeRaw(l["tokenized_raw"], "", yaml_sample_tokens)
            yaml_sample["count"] = i
        elif args.type == "scriptedInputs":
            yaml_sample["count"] = i
            yaml_sample_line["_raw"] = yamlMultilineStr(tokenizeRaw(l["tokenized_raw"], l.get(args.events_splitby), yaml_sample_tokens))
        else:
            yaml_sample_line["_raw"] = yamlMultilineStr(tokenizeRaw(l["tokenized_raw"], l.get(args.events_splitby), yaml_sample_tokens))
        
        yaml_sample_lines.append(yaml_sample_line)

    yaml_sample["lines"] = yaml_sample_lines
    
    processReplacements(args.replacementsCSV, y)

    print(y)
    return (y)


def execute(args):
    return (main(args))