import argparse
import yaml
import copy
import sys

from datapet_yaml_generator_helpers import *
import datapet_yaml_generator_all

def printResults(args, item):
	print ("COMPUTED DATAPET YAMLS")
	print(yaml.safe_dump(item, sort_keys=False))

	#Print out where we failed to insert tokens for them to be updated!
	#if unspecifiedTokens:
	#	print ("WARNING: THE FOLLOWING TOKENS ARE UNREFERENCED IN _RAW SAMPLES.\n_RAW MAY NEED TO BE MANUALLY UPDATED OR TOKEN MANUALLY REMOVED.")
	#	[print(t) for t in unspecifiedTokens]

def main(args):
	resultingYaml = datapet_yaml_generator_all.execute(args)

	#In preview mode, bail out before saving
	printResults(args, resultingYaml)
	if(args.preview):
		return
	
	#Rebuild what the command line was
	fullCommandLine = "python3 "
	for i, v in enumerate(sys.argv):
		v=v.split("=",1)

		if i==0:
			fullCommandLine = fullCommandLine + "{} ".format(v[0])
		elif len(v)>1:
			fullCommandLine = fullCommandLine + "{}=\"{}\" ".format(v[0], v[1])
		else:
			fullCommandLine = fullCommandLine + "\"{}\" ".format(v[0])

	writeYamlProject(args, fullCommandLine, resultingYaml)


parser = argparse.ArgumentParser()

parser.add_argument("splunkInstance", help="URL of splunk instance to search via rest. (I.E. https://ec2-54-183-21-92.us-west-1.compute.amazonaws.com)")
parser.add_argument("splunkUsername", help="Username")
parser.add_argument("splunkPassword", help="Password")
parser.add_argument("type", help="What type of data are we copying? Events, metrics, or Scripted Inputs?", choices=['events', 'scriptedInputs', 'metrics'])
parser.add_argument("index", help="Index(es) to search. Wildcards supported.")
parser.add_argument("sourcetype", help="Sourcetype(s) to search. Wildcards supported.")
parser.add_argument("outputdir", help="Parent folder in which to store the resulting yaml configs.")

parser.add_argument("--metrics_filter", help="Filter down the metrics to collect", default="*")
parser.add_argument("--earliest", help="Earliest timerange", default="-60m@m")
parser.add_argument("--latest", help="Latest Timerange", default="now")
parser.add_argument("--preview", help="If specified, yaml is not written, but just output to screen.", action="store_true")
parser.add_argument("--outputdirroot", help="The root directory on the machine to write this datepet project folder to", default="/tmp")
parser.add_argument("--events_timestampformat", help="The strftime format of the timestamps in the event to find and replace.", default="%m/%d/%Y %H:%M:%S")
parser.add_argument("--events_splitby", help="How to determine what constitutes a new 'type' of event to include in the sample. Defaults to punct.", default="punct")
parser.add_argument("--target_entity_filter", help="Specify how to filter the search to just get data for a single entity. (I.E. host=myhostname)", default="index=*")
parser.add_argument("--fields_to_tokenize", help="A comma separated list of fields in the raw data you want to perform token replacement on", default="")
parser.add_argument("--replacementsCSV", help="foo", default=None)

main(parser.parse_args())

###
# python3 ./datapet_yaml_generator_wrapper.py https://54.183.21.92 admin jeffSplunk metrics "*" "*" ITF-VMWareESXi-Metrics --outputdirroot="~/temp/datapet-sample-gen" --metrics_filter="*esxi*" --earliest=-60m --target_entity_filter="moid=host-29" --preview
